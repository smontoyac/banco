package com.steve.montoya.bl;

public  abstract class Cuenta {
    protected int saldo;
    protected String numero;

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Cuenta{" +
                "saldo=" + saldo +
                ", numero='" + numero + '\'' +
                '}';
    }

    public abstract void depositarMonto();
    public abstract void retirarrMonto();

}
